// Created with Motiva Layama v1.18b https://www.motivacg.com

function getLayamaCameras()
{
   var layamaCameras = new BABYLON.SmartArray(0);
   layamaCameras.push({n: "BedroomLiving_High_VR000005", p: new BABYLON.Vector3(13327.9, 14694.4, 13422.3), l: new BABYLON.Vector3(13327.9, 14694.4, 13422.2)});
   layamaCameras.push({n: "BedroomLiving_High_VR000000", p: new BABYLON.Vector3(12918.9, 14676.8, 12596.9), l: new BABYLON.Vector3(12918.9, 14676.8, 12597)});
   layamaCameras.push({n: "BedroomLiving_High_VR000001", p: new BABYLON.Vector3(12749, 14676.8, 12597.9), l: new BABYLON.Vector3(12749, 14676.8, 12598)});
   layamaCameras.push({n: "BedroomLiving_High_VR000002", p: new BABYLON.Vector3(12960.1, 14676.8, 12884), l: new BABYLON.Vector3(12960.1, 14676.8, 12884.1)});
   layamaCameras.push({n: "BedroomLiving_High_VR000003", p: new BABYLON.Vector3(12796.8, 14676.8, 12868.1), l: new BABYLON.Vector3(12796.8, 14676.8, 12868.2)});
   layamaCameras.push({n: "BedroomLiving_High_VR000004", p: new BABYLON.Vector3(12942.1, 14676.8, 12736.5), l: new BABYLON.Vector3(12942.1, 14676.8, 12736.6)});
   layamaCameras.push({n: "BedroomLiving_High_VR000006", p: new BABYLON.Vector3(13324.4, 14694.4, 13223.2), l: new BABYLON.Vector3(13324.4, 14694.4, 13223.1)});
   layamaCameras.push({n: "BedroomLiving_High_VR000007", p: new BABYLON.Vector3(13327, 14694.4, 13058), l: new BABYLON.Vector3(13327, 14694.4, 13057.9)});
   layamaCameras.push({n: "BedroomLiving_High_VR000008", p: new BABYLON.Vector3(13094.4, 14694.4, 13198.7), l: new BABYLON.Vector3(13094.4, 14694.4, 13198.6)});
   layamaCameras.push({n: "BedroomLiving_High_VR000009", p: new BABYLON.Vector3(13181.5, 14694.4, 13057.3), l: new BABYLON.Vector3(13181.5, 14694.4, 13057.2)});
   layamaCameras.push({n: "BedroomLiving_High_VR000010", p: new BABYLON.Vector3(13331.9, 14694.4, 12908.4), l: new BABYLON.Vector3(13331.9, 14694.4, 12908.3)});
   layamaCameras.push({n: "BedroomLiving_High_VR000011", p: new BABYLON.Vector3(13326.4, 14694.4, 12615.7), l: new BABYLON.Vector3(13326.4, 14694.4, 12615.6)});
   layamaCameras.push({n: "BedroomLiving_High_VR000012", p: new BABYLON.Vector3(13332.8, 14694.4, 12762.1), l: new BABYLON.Vector3(13332.8, 14694.4, 12762)});
   layamaCameras.push({n: "BedroomLiving_High_VR000013", p: new BABYLON.Vector3(13102.6, 14694.4, 12598.1), l: new BABYLON.Vector3(13102.6, 14694.4, 12598.2)});
   layamaCameras.push({n: "BedroomLiving_High_VR000014", p: new BABYLON.Vector3(13102.6, 14694.4, 12823), l: new BABYLON.Vector3(13102.6, 14694.4, 12823.1)});
   layamaCameras.push({n: "BedroomLiving_High_VR000015", p: new BABYLON.Vector3(13033.6, 14694.4, 13009.3), l: new BABYLON.Vector3(13033.6, 14694.4, 13009.2)});
   layamaCameras.push({n: "BedroomLiving_High_VR000016", p: new BABYLON.Vector3(13404.1, 14676.8, 13056.1), l: new BABYLON.Vector3(13404.1, 14676.8, 13056.2)});
   layamaCameras.push({n: "BedroomLiving_High_VR000017", p: new BABYLON.Vector3(13477.7, 14676.8, 13233.3), l: new BABYLON.Vector3(13477.7, 14676.8, 13233.4)});
   layamaCameras.push({n: "BedroomLiving_High_VR000018", p: new BABYLON.Vector3(13652.1, 14676.8, 13233.3), l: new BABYLON.Vector3(13652.1, 14676.8, 13233.4)});
   layamaCameras.push({n: "BedroomLiving_High_VR000019", p: new BABYLON.Vector3(13652.1, 14676.8, 13404.1), l: new BABYLON.Vector3(13652.1, 14676.8, 13404.2)});
   layamaCameras.push({n: "BedroomLiving_High_VR000020", p: new BABYLON.Vector3(13652.1, 14676.8, 12613.3), l: new BABYLON.Vector3(13652.1, 14676.8, 12613.4)});
   layamaCameras.push({n: "BedroomLiving_High_VR000021", p: new BABYLON.Vector3(13652.1, 14676.8, 12734.7), l: new BABYLON.Vector3(13652.1, 14676.8, 12734.8)});
   layamaCameras.push({n: "BedroomLiving_High_VR000022", p: new BABYLON.Vector3(13406.8, 14676.8, 12734.4), l: new BABYLON.Vector3(13406.8, 14676.8, 12734.5)});
   layamaCameras.push({n: "BedroomLiving_High_VR000023", p: new BABYLON.Vector3(13527.1, 14676.8, 12734.4), l: new BABYLON.Vector3(13527.1, 14676.8, 12734.5)});
   layamaCameras.push({n: "BedroomLiving_High_VR000024", p: new BABYLON.Vector3(13527.9, 14676.8, 12613.3), l: new BABYLON.Vector3(13527.9, 14676.8, 12613.4)});
   return layamaCameras;
}

function getLayamaResolutions()
{
   var layamaResolutions = new BABYLON.SmartArray(0);
   layamaResolutions.push("2048");
   layamaResolutions.push("1024");
   return layamaResolutions;
}

function getOnScreenLogoUsage()
{
   return 6;
}

function getLayamaControls()
{
   return {defMove: true, defRot: 0, altMove: true, altRot: 1};
}

